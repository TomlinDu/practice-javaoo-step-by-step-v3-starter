package ooss;

public class Student extends Person {
    protected Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        StringBuilder introduce = new StringBuilder(String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age));
        if (this.klass != null && !this.klass.isLeader(this)) {
            introduce.append(String.format(" I am in class %d.", this.klass.number));
        } else if (this.klass != null && this.klass.leader.equals(this)) {
            introduce.append(String.format(" I am the leader of class %d.", this.klass.number));
        }
        return introduce.toString();
    }

    public void join() {
        join(null);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) return false;
        return this.klass.number == klass.number;
    }




}
